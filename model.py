import numpy as np
import torch.nn as nn
import torch


class SelectItem(nn.Module):
    def __init__(self, item_index):
        super(SelectItem, self).__init__()
        self._name = 'selectitem'
        self.item_index = item_index

    def forward(self, inputs):
        return inputs[self.item_index]


class RT_pred_model(nn.Module):

    def __init__(self, drop_rate):
        super(RT_pred_model, self).__init__()
        self.encoder = nn.Sequential(
            nn.GRU(input_size=8, hidden_size=16, num_layers=2, dropout=drop_rate, bidirectional=True, batch_first=True),
            SelectItem(1),
            nn.Dropout(p=drop_rate)
        )

        self.decoder = nn.Sequential(
            nn.Linear(64, 16),
            nn.ReLU(),
            nn.Dropout(p=drop_rate),
            nn.Linear(16, 8),
            nn.ReLU(),
            nn.Dropout(p=drop_rate),
            nn.Linear(8, 1)
        )

        self.emb = nn.Linear(24, 8)

        self.encoder.double()
        self.decoder.double()
        self.emb.double()

    def forward(self, seq):
        seq = torch.tensor(seq)
        x = torch.nn.functional.one_hot(seq, 24)
        x_emb = self.emb(x.double())
        x_enc = self.encoder(x_emb)
        x_enc = x_enc.swapaxes(0, 1)
        x_enc = torch.flatten(x_enc, start_dim=1)
        x_rt = self.decoder(x_enc)
        x_rt = torch.flatten(x_rt)
        return x_rt


class Intensity_pred_model(nn.Module):

    def __init__(self, drop_rate):
        super(Intensity_pred_model, self).__init__()
        self.seq_encoder = nn.Sequential(
            nn.GRU(input_size=8, hidden_size=256, num_layers=1, dropout=drop_rate, bidirectional=True,
                   batch_first=True),
            SelectItem(1),
            nn.ReLU(),
            nn.Dropout(p=drop_rate),
            nn.GRU(input_size=128, hidden_size=128, num_layers=1, dropout=drop_rate, bidirectional=True,
                   batch_first=True),
            SelectItem(1),
            nn.Dropout(p=drop_rate),
        )

        self.meta_enc = nn.Sequential(nn.Linear(7, 128))

        self.emb = nn.Linear(24, 8)

        self.decoder = nn.Sequential(
            nn.GRU(input_size=128, hidden_size=512, num_layers=1, dropout=drop_rate, bidirectional=True,
                   batch_first=True),
            SelectItem(1),
            nn.Dropout(p=drop_rate),
        )

        self.regressor = nn.Linear(128, 6)

        # intensity range from 0 to 1 (-1 mean impossible)
        self.meta_enc.double()
        self.seq_encoder.double()
        self.decoder.double()
        self.emb.double()

    def forward(self, input_x):
        seq, energy, charge = input_x
        print('sq', seq.size)
        charge = torch.tensor(charge)
        energy = torch.tensor(energy)
        seq = torch.tensor(seq)
        x = torch.nn.functional.one_hot(seq, 24)
        x_emb = self.emb(x.double())
        out_1 = self.seq_encoder(x_emb)
        out_1 = torch.flatten(out_1, start_dim=1)

        # metadata encoder

        out_2 = self.meta_enc(torch.concat([charge, energy]))

        encoding = torch.mul(out_2, out_1)

        input_x = encoding.repeat(6)

        out = self.decoder(input_x)

        return out
