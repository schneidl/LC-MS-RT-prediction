import h5py
import numpy as np
from torch.utils.data import Dataset, DataLoader
import pandas as pd


def padding(dataframe, columns, length):
    def pad(x):
        return x + (length - len(x)) * '_'

    for i in range(len(dataframe)):
        if len(dataframe[columns][i]) > length:
            dataframe.drop(i)
    dataframe[columns] = dataframe[columns].map(pad)


class RT_Dataset(Dataset):

    def __init__(self, data_source, mode, length, format='iRT'):
        self.data = pd.read_csv(data_source, nrows=1000)
        padding(self.data, 'sequence', length)
        if mode == 'train':
            self.data = self.data[self.data.state == 'train']
        elif mode == 'test':
            self.data = self.data[self.data.state == 'holdout']
        elif mode == 'validation':
            self.data = self.data[self.data.state == 'validation']

        self.data = self.data.reset_index()

        self.mode = mode
        self.format = format

    def __getitem__(self, index: int):
        seq = self.data['sequence'][index]
        if self.format == 'RT':
            label = self.data['retention_time'][index]
        if self.format == 'iRT':
            label = self.data['irt'][index]
        if self.format == 'iRT_scaled':
            label = self.data['iRT_scaled'][index]
        if self.format == 'score':
            label = self.data['score'][index]

        return [*seq], label

    def __len__(self) -> int:
        return self.data.shape[0]


def load_data(batch_size, data_source):
    train = RT_Dataset(data_source, 'train', 12)
    test = RT_Dataset(data_source, 'test', 12)
    train_loader = DataLoader(train, batch_size=batch_size)
    test_loader = DataLoader(test, batch_size=batch_size)

    return train_loader, test_loader


class H5ToStorage():
    def __init__(self, hdf_path, ds_name="train"):
        self.path = hdf_path

        self.classes = []
        with h5py.File(hdf_path, 'r') as hf:
            for class_ in hf:
                self.classes.append(class_)

        self.name = ds_name

    def get_class(self):
        return self.classes

    def make_npy_file(self, f_name, column):
        with h5py.File(self.path, 'r') as hf:
            data = hf[column]
            np.save(f_name, data)


def load_split_intensity(sources, split=(0.5, 0.25, 0.25)):
    assert sum(split) == 1, 'Wrong split argument'
    seq = np.load(sources[0])
    masses = np.load(sources[1])
    intensity = np.load(sources[2])
    energy = np.load(sources[3])
    precursor_charge = np.load(sources[4])

    len = np.shape(energy)[0]
    ind1 = int(np.floor(len * split[0]))
    ind2 = int(np.floor(len * (split[0] + split[1])))
    train = (seq[:ind1],masses[:ind1],intensity[:ind1],energy[:ind1],precursor_charge[:ind1])
    validation = (seq[ind1:ind2],masses[ind1:ind2],intensity[ind1:ind2],energy[ind1:ind2],precursor_charge[ind1:ind2])
    test = (seq[ind2:],masses[ind2:],intensity[ind2:],energy[ind2:],precursor_charge[ind2:])

    return train, validation, test


class Intentsity_Dataset(Dataset):

    def __init__(self, data):
        self.data = data
        self.seq = data[0]
        self.masses = data[1]
        self.intensity =data[2]
        self.energy = data[3]
        self.precursor_charge = data[4]

    def __len__(self):
        return len(self.seq)

    def __getitem__(self, idx):
        return (self.seq[idx], self.energy[idx], self.precursor_charge[idx]), self.intensity[idx]


# storage = H5ToStorage('dataset/traintest_hcd.hdf5')
# storage.make_npy_file('data/intensity/sequence.npy','sequence_integer')
# storage.make_npy_file('data/intensity/intensity.npy', 'intensities_raw')
# storage.make_npy_file('data/intensity/collision_energy.npy', 'collision_energy_aligned_normed')
# storage.make_npy_file('data/intensity/precursor_charge.npy', 'precursor_charge_onehot')
