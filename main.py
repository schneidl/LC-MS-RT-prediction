import os

import torch.nn as nn
import torch.optim as optim
import torch
from dataloader import load_data, load_split_intensity, Intentsity_Dataset
from model import RT_pred_model, Intensity_pred_model


def train(model, data_train, epoch, optimizer, criterion, cuda=False):
    losses = 0.
    i = 0
    for data, target in data_train:
        print(len(data))
        # if cuda:
        #     data, target = data.cuda(), target.cuda()
        print(i / len(data_train) * 100, '%')
        pred_rt = model.forward(data)
        target.float()
        loss = criterion(pred_rt, target)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        losses += loss.item()
        i += 32
    print('epoch : ', epoch, ', losses : ', losses / len(data_train))


def eval(model, data_test, epoch, criterion=nn.MSELoss(reduction='mean'), cuda=False):
    losses = 0.
    i = 0
    for data, target in data_test:
        print(i / len(data_test) * 100, '%')
        # if cuda:
        #     data, target = data.cuda(), target.cuda()
        pred_rt = model(data)
        loss = criterion(pred_rt, target)
        losses += loss.item()
        i += 32
    print('epoch : ', epoch, ', losses : ', losses / len(data_test))


def save(model, optimizer, epoch, checkpoint_name):
    print('\nModel Saving...')
    model_state_dict = model.state_dict()

    torch.save({
        'model_state_dict': model_state_dict,
        'global_epoch': epoch,
        'optimizer_state_dict': optimizer.state_dict(),
    }, os.path.join('checkpoints', checkpoint_name))


def run(epochs, model, data_train, data_test, optimizer, criterion=nn.MSELoss(reduction='mean'), cuda=False):
    for e in range(1, epochs + 1):
        train(model, data_train, e, optimizer, criterion, cuda=cuda)
        if e % 5 == 0:
            eval(model, data_test, e, cuda=cuda)
    save(model, optimizer, epochs, 'test')


def main():
    print(torch.cuda.is_available())
    data_train, data_test, data_validation = load_data(32, sources_train=['data/X_train.npy', 'data/Y_train.npy'],
                                                       sources_test=['data/X_holdout.npy', 'data/Y_holdout.npy'],
                                                       sources_validation=['data/X_validation.npy',
                                                                           'data/Y_validation.npy'])
    print('\nData loaded')
    model = RT_pred_model(0.2)
    model = model.cuda()
    optimizer = optim.Adam(model.parameters(), lr=0.001)
    print('\nModel initialised')
    run(1, model, data_train, data_test, optimizer=optimizer, cuda=True)


def main2():
    print(torch.cuda.is_available())
    sources = ('data/intensity/sequence_head.npy',
               'data/intensity/masses_head.npy',
               'data/intensity/intensity_head.npy',
               'data/intensity/collision_energy_head.npy',
               'data/intensity/precursor_charge_head.npy')

    data_train, data_test, data_test = load_split_intensity(sources, (0.5, 0.25, 0.25))

    train = Intentsity_Dataset(data_train)
    test = Intentsity_Dataset(data_test)

    print('\nData loaded')
    model = Intensity_pred_model(0.2)
    # model = model.cuda()
    optimizer = optim.Adam(model.parameters(), lr=0.001)
    print('\nModel initialised')
    run(1, model, train, test, optimizer=optimizer, cuda=True)


if __name__ == 'main':
    main2()
