import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('agg')


def separe_by_length(X, Y, base_name='out'):
    max = X.shape[1]
    datasets = [[[], []] for i in range(max)]
    for i in range(X.shape[0]):

        try:
            datasets[list(X[i]).index(0)][0].append(X[i])
            datasets[list(X[i]).index(0)][1].append(Y[i])
        except:
            datasets[max - 1][0].append(X[i][:])
            datasets[max - 1][1].append(Y[i])

    for length in range(max):
        print('Cutting ', length)
        if datasets[length][0]:
            print('data/X_' + base_name + '_' + str(length) + '.npy')
            X_cut = np.array(datasets[length][0])
            Y_cut = np.array(datasets[length][1])
            np.save('data/X_' + base_name + '_' + str(length) + '.npy', X_cut)
            np.save('data/Y_' + base_name + '_' + str(length) + '.npy', Y_cut)


def dist_long(X, plot=False, save=False, f_name='out.png'):
    max = X.shape[1]
    dist = np.zeros(max)
    for seq in X:
        try:
            dist[list(seq).index(0)] += 1
        except:
            dist[-1] += 1

    if plot or save:

        plt.stairs(dist, range(max + 1), fill=True)
        if plot:
            plt.show()
        if save:
            plt.savefig(f_name)
        plt.clf()
        plt.close()
    return 100 * dist / X.shape[0]


def feq_aa(X, plot=False, save=False, f_name='out.png'):
    freq = np.zeros(22)
    for seq in X:
        for aa in seq:
            freq[aa] += 1
    freq = freq[1:-1]
    freq = 100 * freq / freq.sum()
    ALPHABET_UNMOD = {
        "A": 1,
        "C": 2,
        "D": 3,
        "E": 4,
        "F": 5,
        "G": 6,
        "H": 7,
        "I": 8,
        "K": 9,
        "L": 10,
        "M": 11,
        "N": 12,
        "P": 13,
        "Q": 14,
        "R": 15,
        "S": 16,
        "T": 17,
        "V": 18,
        "W": 19,
        "Y": 20,
    }
    dict_freq = ALPHABET_UNMOD.copy()
    for aa in list(ALPHABET_UNMOD.keys()):
        dict_freq[aa] = freq[ALPHABET_UNMOD[aa] - 1]

    if plot or save:
        plt.bar(list(ALPHABET_UNMOD.keys()), freq, label=list(ALPHABET_UNMOD.keys()))
        if plot:
            plt.show()
        if save:
            plt.savefig(f_name)
        plt.clf()
        plt.close()
    return dict_freq


def intersection(A, B):
    nrows, ncols = A.shape
    dtype = {'names': ['f{}'.format(i) for i in range(ncols)],
             'formats': ncols * [A.dtype]}

    C = np.intersect1d(A.view(dtype), B.view(dtype))

    # This last bit is optional if you're okay with "C" being a structured array...
    C = C.view(A.dtype).reshape(-1, ncols)

    return C


X_train = np.load('data/X_train.npy')
Y_train = np.load('data/Y_train.npy')
X_validation = np.load('data/X_validation.npy')
Y_validation = np.load('data/Y_validation.npy')
X_test = np.load('data/X_holdout.npy')
Y_test = np.load('data/Y_holdout.npy')

separe_by_length(X_test, Y_test, 'test')

print('\n Tailles des données')
print('\n Train : ', Y_train.size)
print('Validation : ', Y_validation.size)
print('Test: ', Y_test.size)

print('\n Longueurs des séquences')
print('\n Train : ', dist_long(X_train, plot=False, save=True, f_name='fig/histo_length_train.png'))
print('Validation : ', dist_long(X_validation, plot=False, save=True, f_name='fig/histo_length_validation.png'))
print('Test : ', dist_long(X_test, plot=False, save=True, f_name='fig/histo_length_test.png'))

print('\n Fréquences des acides aminés')
print('\n Train : ', feq_aa(X_train, plot=False, save=True, f_name='fig/histo_aa_train.png'))
print('Validation : ', feq_aa(X_validation, plot=False, save=True, f_name='fig/histo_aa_validation.png'))
print('Test : ', feq_aa(X_test, plot=False, save=True, f_name='fig/histo_aa_test.png'))

print('\n Intersection checking')
print('\n Train x Validation : ', len(intersection(X_train, X_validation)), ' intersection(s)')
print('Train x Test : ', len(intersection(X_train, X_test)), ' intersection(s)')
print('Test x validation : ', len(intersection(X_test, X_validation)), ' intersection(s)')
